<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\support\facades\DB;

class TestsController extends Controller
{
    public function one () {
        // $results = DB::table('products')
        //         ->value('name');
        // var_dump($results);

        $titles = DB::table('products')
        ->where('id', '2')
        ->value('name');
        dd($titles);
        // foreach ($titles as $title) {
        //     echo $title;
        // }

        return view("tests.one", [
            'foo' => 'bar1',
            'hestage' => 'niddle'
        ]
    ); 
    }
    public function two () {
        return view("tests.two", [
            'foo' => 'bar1',
            'hestage' => 'niddle'
        ]
    );
    }
}
