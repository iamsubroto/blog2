<?php

namespace App\Http\Controllers;

use App\Story;
use Illuminate\Http\Request;

class StoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $stories = Story::where('user_id', auth()->user()->id )
        //     ->orderBy('id', 'DESC')
        //     ->get(); // Query here

        // Pagenate
        $stories = Story::where('user_id', auth()->user()->id )
            ->orderBy('id', 'DESC')
            ->paginate(2); // Query here

        return view('stories.index', [ // How to pass value
            'stories' => $stories,
            'name' => 'MyNamehere'
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('stories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    // public function show( $id ) {
    //     // $story = Story::find( $id );
    //     $story = Story::findOrFail( $id );
    //     dd($story);
    // }

    public function show( Story $story ) {  // Route model binding Do Same as before
        return view( 'stories.show', ['story' => $story]); // Controller_Name.Method_Name
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function edit(Story $story)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Story $story)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Story  $story
     * @return \Illuminate\Http\Response
     */
    public function destroy(Story $story)
    {
        //
    }
}
