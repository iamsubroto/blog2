<?php

namespace App\Http\Controllers;

use App\Story;
use Illuminate\Http\Request;

class StoriesController extends Controller
{
    public function index() {
        // $stories = Story::where('user_id', auth()->user()->id )
        //     ->orderBy('id', 'DESC')
        //     ->get(); // Query here

        // Pagenate
        $stories = Story::where('user_id', auth()->user()->id )
            ->orderBy('id', 'DESC')
            ->paginate(2); // Query here

        return view('stories.index', [ // How to pass value
            'stories' => $stories,
            'name' => 'MyNamehere'
        ]);
    }

    // public function show( $id ) {
    //     // $story = Story::find( $id );
    //     $story = Story::findOrFail( $id );
    //     dd($story);
    // }

    public function show( Story $story ) {  // Route model binding Do Same as before
        return view( 'stories.show', ['story' => $story]); // Controller_Name.Method_Name
    }
    
}
